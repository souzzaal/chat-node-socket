const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const path = require('path')

app.get('/', (req, res) => {
	res.sendFile(path.resolve(__dirname, "../public/index.html"));
});

var usersOnline = []

io.on('connection', (socket) => {

	socket.on('join', user => {
		console.log('new user', user)

		let response = {
			joined: false
		}

		if (user.name) {
			usersOnline.push(user)
			response.joined = true;
		}

		console.log(usersOnline)

		socket.emit('joinResponse', response)
		io.emit('updateUsersOnline', usersOnline)
	})

	socket.on('disconnect', () => {
		console.log('disconnect', socket.id)
		usersOnline = usersOnline.filter(user => user.id !== socket.id)
		io.emit('updateUsersOnline', usersOnline)
	});

	socket.on('sendMessage', (msg) => {
		io.emit('newMessage', msg);			// io.emit envia a mensagem para todos incluindo o emissor
	})

	socket.on('typing', someone => {
		socket.broadcast.emit('someoneTyping', `${someone} está digitando... `) // io.broadcast.emit envia a mensagem para todos exceto o emissor
	})
});

http.listen(3000, () => {
	console.log('listening on *:3000');
});